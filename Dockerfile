# Use the official image as parent
FROM jupyter/scipy-notebook


USER $NB_UID

# Install Extra Python Packages
# -----------------------------

# Install packages in requirements.txt
# .....................................
#
# REF: https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#add-or-copy
#

# Note: default Dockerfiles for jupyter/scipy-notebook use conda instead of pip.
#       Use conda packages to ensure consistency.

COPY requirements.txt /tmp/
RUN conda install --yes --file /tmp/requirements.txt && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER


# Ensure switched back to jovyan to avoid accidental container runs as root
USER $NB_UID
