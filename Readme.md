# Dockerfile to build custom jupyter-notebook image for development

## Introduction

For adding packages to the default jupyter-notebook docker images, the preferred method is to issue a pip or conda install at the jupyter command prompt, ie:
```
!pip install <package>
# -or-
!conda install <package>
```

Installing at the prompt ensures the use of a traceable image and is therefore preferred.  However, if one has a number of other packages that are used on a regular basis, there may be a need to build these packages into a custom container for development purposes.  This Dockerfile may be used to add additional
packages to the docker image using conda.  Conda is used for consistency, as the official jupyter-notebook image uses conda to install packages.


## Customization:

The python packages are installed using conda into the docker image.
In order to customize the packages available, edit the 
`requirements.txt` file prior to building the image.

A listing of the current packages installed in the official jupyter-docker-stacks images can be found by running `!conda list` at the jupyter notebook prompt.

The Dockerfile is configured to build upon the official 
docker image for jupyter/scipy-notebook.
It may be desired to build using a different image as a base image.
The various jupyter images available are listed at:  
http://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html

To change the parent image, modify the Dockerfile accordingly, i.e.
modify the line:
```
FROM jupyter/scipy-notebook
```

## Build:
Build the docker image using:

```
docker build -t <IMAGE NAME> .
```
where `<IMAGE NAME>` is the image tag name for the docker image, i.e. `jupyter-scipy-custom`.

A Makefile is provided to simplify the process of building.

## Run:

Run the docker image.  Complete details on the options are found at the jupyter-docker-stacks documentation:

http://jupyter-docker-stacks.readthedocs.io/en/latest/using/common.html#docker-options

Note:  The options below include the option `NB_UID`. This is used
to set the user ID of the mounted volume.

1. Run the image, remove the container on exit.  Additionally, __mount the present working directory to a mount point within the container__.  The `--user` argument and environment variable `NB_UID` is passed to the docker run so that any files modified or created inside the container in the mounted directory from the host will have the proper user credentials.
```
docker run --rm \
     -p 8888:8888
     --user root  \
     -e NB_UID=`id -u` \
     --mount type=bind,source="$PWD",target=/home/jovyan/work \
     <IMAGE NAME>
``` 

## Permissions:

Note that the default image will use a group user ID for `users`.  Thus, in the mounted work directory, newly created notebooks will be of group `users`.
The command line option of ``` -e NB_GID=`id -g` ``` will be useful.  Yet,
at the time of this writing, use of both the NB_GID option and the NB_UID option
prevents installation of packages at the prompt using `!conda` or `!pip`.
Either the group ID or the user ID needs to be at the default value in order
to have proper write permissions to make package installations at the jupyter
notebook prompt using `!conda install <package>`.


