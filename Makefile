# Build the Docker image

# Docker image name (tag)
TAG=jupyter-scipy-custom

image: Dockerfile requirements.txt
	docker build -t $(TAG) .
